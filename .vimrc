" VIM Configuration
" Symlinked by NeoVim


" Set Leader
let mapleader=","

set nocompatible " required
filetype off 	 " required
syntax enable

set noerrorbells
set expandtab
set smartindent
set incsearch
set clipboard=unnamed,unnamedplus
set noshowmode
set undofile " enables persistent undo
set undodir=$HOME/.vimundo " undo buffer dir
set number
set cursorline

call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Make sure you use single quotes
Plug 'flazz/vim-colorschemes'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdcommenter'
Plug 'preservim/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'wincent/terminus'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'

call plug#end()


""""""""""""""""""""""
"" Navigation
"""""""""""""""""""""
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
set ttimeoutlen=1
set listchars=tab:>-,trail:~,extends:>,precedes:<,space:.
set ttyfast


"""""""""""""""""""""""
"" Editor Settings
"""""""""""""""""""""""
if (has("termguicolors"))
 set termguicolors
endif

let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Theme
colorscheme gruvbox

" Line Numbers
set number

" Cursor Line
highlight Cursor guifg=reverse guibg=white
highlight iCursor guifg=reverse guibg=white
highlight Visual ctermfg=16 guifg=Black ctermbg=11 
 
" Airline
let g:airline_theme='base16_gruvbox_dark_hard'

"""""""""""""""""""""""
"" Mappings
"""""""""""""""""""""""
nmap <leader>r :so %<ENTER>

" NerdTree Toggle
nmap <leader>ne :NERDTreeToggle<CR>


"""""""""""""""""""""""

" Open NerdTree on startup
" autocmd vimenter * NERDTree

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

