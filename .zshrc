# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Specify default editor
export EDITOR=nvim
export GIT_EDITOR=vim

# Plugins
plugins=(git tmuxinator zsh-autosuggestions autojump zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh
source $HOME/.functions

# Initialize Pure
# https://github.com/sindresorhus/pure
fpath+=$HOME/.zsh/pure # M1 path workaround
autoload -U promptinit; promptinit

PURE_GIT_PULL=1 # Prevents Pure from checking whether the current Git remote has been updated.
PURE_GIT_UNTRACKED_DIRTY=1 # Do not include untracked files in dirtiness check. Mostly useful on large repos

PURE_PROMPT_SYMBOL="»"
PURE_PROMPT_VICMD_SYMBOL="«"

prompt pure

# Theme
ZSH_THEME=""

# Custom Plugin Settings
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#665c54,bg=#3c3836,underline"

# FZF
[ -f ~/.fzf.sh ] && source ~/.fzf.sh
export FZF_DEFAULT_OPS="--extended"
export FZF_CTRL_R_OPTS="-i"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Load Pyenv
eval "$(pyenv init -)"

# SCM Breeze
[ -s "/Users/willwung/.scm_breeze/scm_breeze.sh" ] && source "/Users/willwung/.scm_breeze/scm_breeze.sh"

source /opt/homebrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Aliases should be defined at the end of the file
source $HOME/.aliases

# Unalias grv in oh-my-zsh to use the GRV client
# https://github.com/rgburke/grv
unalias grv

# Required for pipx autocompletion
autoload -U bashcompinit
bashcompinit


# bun completions
[ -s "/Users/willwung/.bun/_bun" ] && source "/Users/willwung/.bun/_bun"

# Bun
export BUN_INSTALL="/Users/willwung/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
# 

# asdf
. /opt/homebrew/opt/asdf/libexec/asdf.sh
