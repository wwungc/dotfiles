# .dotfiles

Dot files are managed by [YADM](https://github.com/TheLocehiliosan/yadm).

## Cloning these dotfiles on a new machine

```
yadm clone git@gitlab.com:wwungc/dotfiles.git
```

## Pulling remote changes to dotfiles

```
yadm pull --rebase origin master
```

## Pushing

```
yadm add <file>
yadm commit
yadm push
```

